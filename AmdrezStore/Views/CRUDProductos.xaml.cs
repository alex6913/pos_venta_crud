﻿using System.Windows;
using System.Windows.Controls;

namespace AmdrezStore.Views
{
    public partial class CRUDProductos : Page
    {
        public CRUDProductos()
        {
            InitializeComponent();
        }

        #region Regresar

        private void Regresar(object sender, RoutedEventArgs e)
        {
            Content = new Productos();
        }

        #endregion

        #region CRUD

        #region Create

        private void Crear(object sender, RoutedEventArgs e)
        {

        }

        #endregion

        #region Delete

        private void Eliminar(object sender, RoutedEventArgs e)
        {

        }

        #endregion

        #region Update

        private void Actualizar(object sender, RoutedEventArgs e)
        {

        }

        #endregion

        #endregion

        #region Subir Imagen

        private void Subir(object sender, RoutedEventArgs e)
        {

        }

        #endregion
    }
}
