﻿using System.Windows;
using System.Windows.Controls;

namespace AmdrezStore.Views
{
    public partial class Productos : UserControl
    {
        public Productos()
        {
            InitializeComponent();
        }

        #region Buscando

        private void Buscando(object sender, TextChangedEventArgs e)
        {

        }

        #endregion

        #region CRUD

        #region Create

        private void Agregar_Producto(object sender, RoutedEventArgs e)
        {
            CRUDProductos ventana = new CRUDProductos();
            FrameProductos.Content = ventana;
            Contenido.Visibility = Visibility.Hidden;
            ventana.BtnCrear.Visibility = Visibility.Visible;
        }

        #endregion

        #region Read

        private void Consultar(object sender, RoutedEventArgs e)
        {

        }

        #endregion

        #region Update

        private void Actualizar(object sender, RoutedEventArgs e)
        {

        }

        #endregion

        #region Delete

        private void Eliminar(object sender, RoutedEventArgs e)
        {

        }

        #endregion

        #endregion
    }
}
